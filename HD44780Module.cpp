#include <HD44780Module.h>

void HD44780Module::setPin(byte pin, bool data) {
	pinMode(pin, OUTPUT);
	digitalWrite(pin, data);
	delayMicroseconds(HD44780Module::DELAY_MICROSECONDS);
}

bool HD44780Module::getPin(byte pin) {
	pinMode(pin, INPUT);
	bool data = digitalRead(pin);
	delayMicroseconds(HD44780Module::DELAY_MICROSECONDS);
	return data;
}

void HD44780Module::writeByte(byte data) {
	this->setPin(this->enPin, HIGH);
	for (byte i = 0; i < HD44780Module::LENGTH; i++) {
		if (this->dPins[i] < HD44780Module::UNUSED) {
			this->setPin(this->dPins[i], (data & (1 << i)) > 0);
		}
	}
	this->setPin(this->enPin, LOW);
}

byte HD44780Module::readByte() {
	this->setPin(this->enPin, HIGH);
	byte data = 0;
	for (byte i = 0; i < HD44780Module::LENGTH; i++) {
		if (this->dPins[i] < HD44780Module::UNUSED) {
			if (this->getPin(this->dPins[i])) {
				data |= (1 << i);
			}
		}
	}
	this->setPin(this->enPin, LOW);
	return data;
}

void HD44780Module::writeRegister(bool rs, byte data) {
	this->setPin(this->rsPin, rs);
	this->setPin(this->rwPin, LOW);
	this->writeByte(data);
	if (!this->bits) {
		this->writeByte((data & 0x0F) << 4);
	}
}

byte HD44780Module::readRegister(bool rs) {
	this->setPin(this->rsPin, rs);
	this->setPin(this->rwPin, HIGH);
	byte data = this->readByte();
	if (!this->bits) {
		data = (data & 0xF0) | (this->readByte() >> 4);
	}
	return data;
}

void HD44780Module::setMovement(bool type, bool direction) {
	byte data = 0x10;
	if (type) {
		data |= 0x08;
	}
	if (direction) {
		data |= 0x04;
	}
	this->writeRegister(LOW, data);
}

void HD44780Module::setFunction(bool rows, bool dots) {
	byte data = 0x20;
	if (this->bits) {
		data |= 0x10;
	}
	if (rows) {
		data |= 0x08;
	}
	if (dots) {
		data |= 0x04;
	}
	this->writeRegister(LOW, data);
}

HD44780Module::HD44780Module(byte rsPin, byte rwPin, byte enPin, byte d0Pin, byte d1Pin, byte d2Pin, byte d3Pin, byte d4Pin, byte d5Pin, byte d6Pin, byte d7Pin) {
	this->rsPin = rsPin;
	this->rwPin = rwPin;
	this->enPin = enPin;
	this->bits
		= ((this->dPins[0] = d0Pin) < HD44780Module::UNUSED)
		&& ((this->dPins[1] = d1Pin) < HD44780Module::UNUSED)
		&& ((this->dPins[2] = d2Pin) < HD44780Module::UNUSED)
		&& ((this->dPins[3] = d3Pin) < HD44780Module::UNUSED);
	this->dPins[4] = d4Pin;
	this->dPins[5] = d5Pin;
	this->dPins[6] = d6Pin;
	this->dPins[7] = d7Pin;
}

HD44780Module::HD44780Module(byte rsPin, byte rwPin, byte enPin, byte d4Pin, byte d5Pin, byte d6Pin, byte d7Pin) :
HD44780Module::HD44780Module(rsPin, rwPin, enPin, HD44780Module::UNUSED, HD44780Module::UNUSED, HD44780Module::UNUSED, HD44780Module::UNUSED, d4Pin, d5Pin, d6Pin, d7Pin) {
}

void HD44780Module::initial(bool rows, bool dots) {
	pinMode(this->rsPin, OUTPUT);
	pinMode(this->rwPin, OUTPUT);
	pinMode(this->enPin, OUTPUT);
	this->setFunction(false, false);
	this->setFunction(rows, dots);
}

void HD44780Module::clear() {
	this->writeRegister(LOW, 0x01);
}

void HD44780Module::home() {
	this->writeRegister(LOW, 0x02);
}

void HD44780Module::setEntry(bool direction, bool type) {
	byte data = 0x04;
	if (direction) {
		data |= 0x02;
	}
	if (type) {
		data |= 0x01;
	}
	this->writeRegister(LOW, data);
}

void HD44780Module::cursorMovePrevious() {
	this->setMovement(HD44780Module::CURSOR, HD44780Module::PREVIOUS);
}

void HD44780Module::cursorMoveNext() {
	this->setMovement(HD44780Module::CURSOR, HD44780Module::NEXT);
}

void HD44780Module::screenMovePrevious() {
	this->setMovement(HD44780Module::SCREEN, HD44780Module::PREVIOUS);
}

void HD44780Module::screenMoveNext() {
	this->setMovement(HD44780Module::SCREEN, HD44780Module::NEXT);
}

void HD44780Module::setDisplay(bool screen, bool cursor, bool blink) {
	byte data = 0x08;
	if (screen) {
		data |= 0x04;
	}
	if (cursor) {
		data |= 0x02;
	}
	if (blink) {
		data |= 0x01;
	}
	this->writeRegister(LOW, data);
}

void HD44780Module::setCharacter(byte address, byte data[HD44780Module::LENGTH]) {
	this->writeRegister(LOW, 0x40 | ((address & 0x07) << 3));
	for (byte i = 0; i < HD44780Module::LENGTH; i++) {
		this->setCode(data[i]);
	}
}

void HD44780Module::setPosition(byte row, byte column) {
	byte data = 0x80;
	if (row > 0) {
		data |= 0x40;
	}
	if (column > 39) {
		column = 39;
	}
	this->writeRegister(LOW, data + column);
}

void HD44780Module::setPosition(byte position) {
	this->setPosition((position & 0x40) > 0 ? 1 : 0, position & 0x3F);
}

void HD44780Module::setCode(byte data) {
	this->writeRegister(HIGH, data);
}

void HD44780Module::setText(String text) {
	const unsigned int LENGTH = text.length();
	for (unsigned int i = 0; i < LENGTH; i++) {
		this->setCode(text.charAt(i));
	}
}

byte HD44780Module::getPosition() {
	return this->readRegister(LOW);
}

byte HD44780Module::getText() {
	return this->readRegister(HIGH);
}