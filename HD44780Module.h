#ifndef HD44780MODULE_H
#define HD44780MODULE_H

#include <Arduino.h>

class HD44780Module {
	private:
		static const unsigned int DELAY_MICROSECONDS = 500;
		static const byte UNUSED = 255;
		static const byte LENGTH = 8;
		bool bits;
		byte rsPin;
		byte rwPin;
		byte enPin;
		byte dPins[HD44780Module::LENGTH];
		void setPin(byte, bool);
		bool getPin(byte);
		void writeByte(byte);
		byte readByte();
		void writeRegister(bool, byte);
		byte readRegister(bool);
		void setMovement(bool, bool);
		void setFunction(bool, bool);
	public:
		static const bool ROWS_1 = false;
		static const bool ROWS_2 = true;
		static const bool DOTS_8 = false;
		static const bool DOTS_10 = true;
		static const bool LEFT = false;
		static const bool RIGHT = true;
		static const bool PREVIOUS = false;
		static const bool NEXT = true;
		static const bool CURSOR = false;
		static const bool SCREEN = true;
		HD44780Module(byte, byte, byte, byte, byte, byte, byte, byte, byte, byte, byte);
		HD44780Module(byte, byte, byte, byte, byte, byte, byte);
		void initial(bool, bool);
		void clear();
		void home();
		void setEntry(bool, bool);
		void cursorMovePrevious();
		void cursorMoveNext();
		void screenMovePrevious();
		void screenMoveNext();
		void setDisplay(bool, bool, bool);
		void setCharacter(byte, byte[HD44780Module::LENGTH]);
		void setPosition(byte, byte);
		void setPosition(byte);
		void setCode(byte);
		void setText(String);
		byte getPosition();
		byte getText();
};

#endif