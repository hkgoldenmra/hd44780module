#include <HD44780Module.h>

HD44780Module hd44780Module = HD44780Module(12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
//HD44780Module hd44780Module = HD44780Module(12, 11, 10, 5, 4, 3, 2);
String text = "hello, world";

void setup() {
	Serial.begin(9600);
	hd44780Module.initial(HD44780Module::ROWS_2, HD44780Module::DOTS_8);
	hd44780Module.setDisplay(true, true, true);
	hd44780Module.setEntry(HD44780Module::RIGHT, HD44780Module::CURSOR);
	hd44780Module.clear();
	hd44780Module.setText(text);
}

void loop() {
	delay(1000);
	hd44780Module.home();
	const unsigned int LENGTH = text.length();
	for (unsigned int i = 0; i < LENGTH; i++) {
		delay(1000);
		String s = String(hd44780Module.getPosition());
		byte data = hd44780Module.getText();
		s += " = ";
		s += ((char) data);
		s += " = ";
		s += data;
		Serial.println(s);
	}
	delay(1000);
	Serial.println();
}