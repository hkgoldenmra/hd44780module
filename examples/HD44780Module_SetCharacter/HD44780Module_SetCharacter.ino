#include <HD44780Module.h>

HD44780Module hd44780Module = HD44780Module(12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
//HD44780Module hd44780Module = HD44780Module(12, 11, 10, 5, 4, 3, 2);

void setup() {
	hd44780Module.initial(HD44780Module::ROWS_2, HD44780Module::DOTS_8);
	hd44780Module.setDisplay(true, false, false);
	hd44780Module.setEntry(HD44780Module::RIGHT, HD44780Module::CURSOR);
	for (byte i = 0; i < 8; i++) {
		byte data[] = {0x0E, 0x1B, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11};
		for (byte j = 8 - i; j < 8; j++) {
			data[j] |= 0x0E;
		}
		hd44780Module.setCharacter(i, data);
	}
	hd44780Module.clear();
	hd44780Module.setText("Battery:");
	for (byte i = 0; i < 8; i++) {
		hd44780Module.setCode(i);
	}
	hd44780Module.setPosition(1, 2);
	hd44780Module.setText("Charging...");
	hd44780Module.setCode(0);
}

void loop() {
	for (byte i = 0; i < 8; i++) {
		delay(250);
		hd44780Module.setPosition(1, 13);
		hd44780Module.setCode(i);
	}
}